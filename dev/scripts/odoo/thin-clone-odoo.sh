#!/usr/bin/env bash

PS3='Odoo version to clone: '
options=("10" "11" "12" "13", "14", "15", "16")
select opt in "${options[@]}";
do
  case $opt in
    "10" | "11" | "12" | "13" | "14" | "15" | "16")
      echo
      break
      ;;
    *) echo "invalid option"
      exit 1
  esac
done

echo "IMPORTANT:"
echo "Make sure the target directory is empty before continuing!"
echo "If you dont, the script will fail."
echo

read -p "Clone Odoo $opt source to $(pwd) (y/N)" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
  git clone --depth=1 --branch="$opt.0" git@github.com:odoo/odoo.git "$(pwd)/$opt"
  rm -rf "$(pwd)/$opt/.git"
  rm -rf "$(pwd)/$opt/.github"
  chmod -R -w "$(pwd)/$opt"
	exit 0
else
  echo "No? OK."
	exit 0
fi