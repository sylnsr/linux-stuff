#!/usr/bin/env bash

cp ~/Downloads/swagger.yaml ./resources/swagger/
# remove the file so that next time its downloaded, it can have the same name
rm ~/Downloads/swagger.yaml