var https = require('https');
var util = require('util');

exports.handler = function (event, context) {
    // console.log(JSON.stringify(event, null, 2));
    // console.log(JSON.stringify(context, null, 2));
    // console.log('From SNS:', event.Records[0].Sns.Message);

    var config = {
        webHookUrlPart: '******************************'
    }

    var postData = {
        "channel": "#aws",
        "username": "AWS SNS",
        "text": "*" + event.Records[0].Sns.Subject + "*",
        "icon_emoji": ":bell:"
    };

    var message = JSON.parse(event.Records[0].Sns.Message);
    var text =
        // "Time: " + message.StateChangeTime + "\n" +
        "Alarm: " + message.AlarmName + "\n" +
        // "Desc: " + message.AlarmDescription + "\n" +
        "State change: " + message.OldStateValue + " to " + message.NewStateValue + "\n" +
        // "Log Group: " + context.logGroupName + "\n" +
        "Region: " + message.Region
    ;

    postData.attachments = [
        {
            "text": text
        }
    ];

    var options = {
        method: 'POST',
        hostname: 'hooks.slack.com',
        port: 443,
        path: '/services' + config.webHookUrlPart
    };

    var req = https.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            context.done(null);
        });
    });

    req.on('error', function (e) {
        console.log('problem with request: ' + e.message);
    });

    req.write(util.format("%j", postData));
    req.end();
};