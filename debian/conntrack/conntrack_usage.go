///bin/true; exec /usr/bin/env go run "$0" "$@"
package conntrack

import (
	"fmt"
	"os/exec"
	"strings"
	"strconv"
)

func main() {
	if o, e := exec.Command("/sbin/sysctl", "net.netfilter.nf_conntrack_count").Output(); e != nil {
		fmt.Println(e.Error())
	} else {
		count := strings.Trim((strings.Split(string(o[:]), ` = `))[1], "\n")
		if o, e := exec.Command("/sbin/sysctl", "net.netfilter.nf_conntrack_max").Output(); e != nil {
			fmt.Println(e.Error())
		} else {
			max := strings.Trim((strings.Split(string(o[:]), ` = `))[1], "\n")
			i_count, e := strconv.Atoi(count)
			if e != nil {
				fmt.Println(e.Error())
			}
			i_max, e := strconv.Atoi(max)
			if e != nil {
				fmt.Println(e.Error())
			}
			i_usage := int(float32(i_count) / float32(i_max) * 100)
			if i_usage > 60 {
				fmt.Println(`nf_conntrack usage exceeds 60%`)
			} else {
				fmt.Println(`nf_conntrack usage <= 60%`)
			}
		}
	}
}
