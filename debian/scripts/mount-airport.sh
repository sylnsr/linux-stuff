#!/usr/bin/env bash

COUNT="$(ls /media/airport | wc -l)"
if [ $COUNT -eq 0 ]; then
    echo "Mounting cifs share: /media/airport ..."
    read -p "cifs share user name: " USERNAME
    read -p "cifs share password: " PASSWORD
    sudo mount.cifs //10.0.0.1/airport /media/airport -o username="$USERNAME",password="$PASSWORD",sec=ntlm,uid="$USER",file_mode=0644,vers=1.0;
fi