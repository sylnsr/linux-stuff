# For Git and colors, in ~/.bashrc, where you have:
# if [ "$color_prompt" = yes ]; then
# .. replace the PS1= line with this one:
PS1="\[\033[1;36m\]\u@\[\033[1;33m\]\h:\[\033[1;31m\]\W\[\033[1;32m\]\$(git branch 2> /dev/null | grep -e ^* | sed -E s/^\\\\\*\ \(.+\)$/\(\\\\\1\)\ /)$ \[\033[m\]"
