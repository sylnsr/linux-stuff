#!/usr/bin/env bash

alias gitserem=git\ remote\ set-url\ origin
alias gitrebasetheirs=git\ pull\ --rebase\ -X\ theirs

gitmatchdev () {
	git pull --all && git merge dev -m"merge dev" && git push
	# I like to go back to dev at this point ... where I work, so:
	git checkout dev
}

gitmergetomain () {
	git checkout main
	gitmatchdev
}

gitmergetoqa () {
	git checkout qa
	gitmatchdev
}

gitmergetopro () {
	git checkout pro
	gitmatchdev
}

gitdestroyfile () {
	# https://stackoverflow.com/questions/872565/remove-sensitive-files-and-their-commits-from-git-history
	git filter-branch --force --index-filter "git update-index --remove $1"
	git push --force --verbose --dry-run
	echo "Next, do:"
	echo "git push --force"
	echo
}

gitprune () {
	git remote prune origin
}
