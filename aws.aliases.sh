#!/usr/bin/env bash

# validate a cloudformation template and echo the status code
cfvalidate () {
	cmd="aws cloudformation validate-template --template-body file:///$(pwd)/$1"
	$cmd
	echo "exit code = $?"
}

cfcreate () {
	cmd="aws cloudformation create-stack --stack-name $1 \
	--capabilities CAPABILITY_IAM \
	--template-body file:///$(pwd)/$2"
	$cmd
	echo "exit code = $?"
}

cfdescribe () {
	cmd="aws cloudformation describe-stacks"
	$cmd
	echo "exit code = $?"
}

cfdelete () {
	cmd="aws cloudformation delete-stack --stack-name $1"
	$cmd
	echo "exit code = $?"
}
