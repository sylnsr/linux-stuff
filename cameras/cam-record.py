#!/usr/bin/env python3
import sys
import os
import time
import datetime

if len(sys.argv) != 3:
    print("Usage: cam-record.py <cam#> <duration (seconds)>")
    sys.exit(1)
if not sys.argv[2].isdigit():
    print("cam# needs to be a digit")
    sys.exit(2)
if not sys.argv[2].isdigit():
    print("duration needs to be an integer")
    sys.exit(2)

CAM_ID = sys.argv[1]
# per day folder name will limit video storage to 7 folders and save space compared,
# to having a folder for each day of the month, or more, for every day of the year
DATE = datetime.datetime.fromtimestamp(time.time()).strftime('%a')  # -%b-%d-%Y
TIME = datetime.datetime.fromtimestamp(time.time()).strftime('%H_%M')
DURATION = sys.argv[2]
DIR = "/media/shared/cams/%s" % DATE.lower()
FNAME = "%s_cam%s" % (TIME, CAM_ID)
PATH = DIR + "/" + FNAME
URL = "rtsp://cam:@10.0.1.15%s:554/11" % CAM_ID
print("Record from %s for %s seconds" % (URL, DURATION))

if not os.path.isdir(DIR):
    try:
        os.makedirs(DIR)
    except:
        print("Failed to create dir: " + DIR)
        sys.exit(3)


#CMD = "ffmpeg -i %s -r 5 -crf 25 -an -y -t %i %s.mp4"
CMD = "ffmpeg -y -i %s -c:v copy -an -t %i %s.mp4"

print("\n")
print(CMD % (URL, int(DURATION), PATH))
print("\n")

os.system(CMD% (URL, int(DURATION), PATH))
