#!/usr/bin/env bash

IN_PATH="$(realpath "$1")"
FIND_PATTERN="*$2.mp4"
FOUND_FILE_LIST="$1/$2.txt"
FINAL_FILE="$IN_PATH/$2.mp4"

find $IN_PATH -type f -name ${FIND_PATTERN} -exec printf "file '%s'\n" {} \; | sort > "$FOUND_FILE_LIST" && \
ffmpeg -f concat -safe 0 -i "$FOUND_FILE_LIST" -c copy "$FINAL_FILE" && \
mv $FINAL_FILE $IN_PATH/$2.tmp && \
rm $IN_PATH/*$2.mp4 && \
rm $IN_PATH/*.txt && \
mv $IN_PATH/$2.tmp $FINAL_FILE