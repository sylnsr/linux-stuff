#!/usr/bin/env bash

# list active container IDs only
drlist () { docker ps -a -q; }

# list images
alias drims="docker images | sort | grep -v REPOSITORY"

# list all containers
alias dracs="docker ps -a"

# restart all containers
dresta () { docker restart $(drlist); }

# restart container _____
alias drest="docker restart"

# kill all running containers
drkilla () { docker kill $(drlist); }

# stop all containers
drstopa () { docker stop $(drlist); }

# get the IP for container _____
alias drip="docker inspect --format '{{ .NetworkSettings.IPAddress }}'"

# get the IP for the last started container
dripl () { drip $(docker ps -l -q); }

# docker-compose
alias drcom="docker compose"

# ECR authenitication
ecrlogin () { $(aws ecr get-login --no-include-email --region us-west-2); }