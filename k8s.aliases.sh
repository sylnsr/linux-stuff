#!/usr/bin/env bash

alias k=kubectl
complete -o default -F __start_kubectl k

# If using microk8s, create an alias for "kubectl" with this command: sudo snap alias microk8s.kubectl kubectl
# See: https://kubernetes.io/blog/2019/11/26/running-kubernetes-locally-on-linux-with-microk8s/#using-microk8s
# and: https://microk8s.io/docs/addon-dashboard

ku-settoken () {
  kubectl create token default
}

ku-showtoken () {
  kubectl -n kube-system describe secret $token
}

ku-cluster () {
  kubectl cluster-info
}

ku-nodes () {
  kubectl get nodes
}

ku-pods () {
  kubectl get pods
}

ku-deps () {
  kubectl get deployments
}

ku-all () {
  kubectl get all --all-namespaces
}

ku-pv () {
  kubectl get persistentvolumes
}

ku-pvc () {
  kubectl get persistentvolumeclaims
}
