#!/bin/bash

# XFCE has an issues with setting up dual monitors, but there is a solution:
# Source: http://bit.ly/1Mg8oUN

# RESOLUTION SETTINGS
# This sets your VGA1 monitor to its best resolution.
xrandr --output VGA-0 --mode 1920x1080 --rate 60
# This sets your laptop monitor to its best resolution.
xrandr --output VGA-1 --mode 1920x1080 --rate 60

# MONITOR ORDER
# Put VGA-1 monitor left of VGA-0
xrandr --output VGA-0 --left-of VGA-1

# PRIMARY MONITOR
# This sets your laptop monitor as your primary monitor.
xrandr --output VGA-1 --primary
